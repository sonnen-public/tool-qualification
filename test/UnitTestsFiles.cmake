set(TARGET_ROOT ${CMAKE_CURRENT_SOURCE_DIR}/../src)
set(TEST_SRC ${CMAKE_CURRENT_SOURCE_DIR}/src)

include_directories(
    ${TARGET_ROOT}
)

# test target files
set( TARGET_SRC
    ${TARGET_ROOT}/sample.cpp
)

file(GLOB_RECURSE TESTS
    "*.cpp"
)

set( TEST_FILES
     ${TARGET_SRC}
     ${TESTS}
)

