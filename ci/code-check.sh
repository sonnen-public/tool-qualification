#!/bin/bash -e
SCRIPT_DIR_ABS=$(realpath $(dirname $0))
ROOT=$(realpath ${SCRIPT_DIR_ABS}/..)

WORK_DIR=${ROOT}/temp/static-analysis
rm -rf ${WORK_DIR}
mkdir -p ${WORK_DIR}
cd ${WORK_DIR}

TARGET_DIRS+=( ${ROOT}/src )

set +e
cppcheck --quiet \
        --platform=unix32 \
        --inconclusive \
        --error-exitcode=1 \
        --force \
        --xml \
        --xml-version=2 \
        ${TARGET_DIRS[@]} 2> report.xml
ERROR_CODE=$?
set -e

REPORT_DIR=/tmp/reports
mkdir -p ${REPORT_DIR}

if [ ${ERROR_CODE} != 0  ] ; then
  cppcheck-htmlreport --file=report.xml \
                      --report-dir=${REPORT_DIR}
  echo
  echo "  !! ERROR FOUND SEE REPORTS !!"
  echo
else
  echo
  echo "  NO ERROR FOUND"
  echo
fi

exit ${ERROR_CODE}

