#!/bin/bash -e
PROJ_ROOT=$(realpath "$(dirname $0)""/..")
TEST_ROOT=${PROJ_ROOT}/test
WORK_DIR=${PROJ_ROOT}/temp/unit-test
rm -rf ${WORK_DIR}
mkdir -p ${WORK_DIR}
cd ${WORK_DIR}
cmake ${TEST_ROOT}
make clean
make -j $(nproc) 
./unittest --gtest_output="xml:report.xml"
lcov --capture --directory CMakeFiles/unittest.dir/${PROJ_ROOT}  --output-file coverage.info

# Put the following line to [Settings] / [CI/CD] / [Test coverage parsing]
# lines.*:\W*(\d+.\d+)%
genhtml coverage.info --output-directory coverage-report --no-prefix | tee lcov.txt
COVERAGE=$(cat lcov.txt | grep 'lines' | grep -oP '(\d+.\d+)')

if [ -z ${THRESHOLD} ] ; then
  THRESHOLD=80.0
fi

IS_TEST_COVERAGE_ENOUGH=$(echo "${COVERAGE} ${THRESHOLD}" | python -c 'import sys; s=sys.stdin.readline(); print(float(s.split()[0]) >= float(s.split()[1]))') 
if [ "${IS_TEST_COVERAGE_ENOUGH}" == "True" ] ;  then
  echo
  echo "Test coverage is good. ( ${COVERAGE} >= ${THRESHOLD} )"
  echo
else
  echo
  echo "Test coverage does not reach the threshold ( ${COVERAGE} < ${THRESHOLD} )"
  echo
  exit 1
fi
