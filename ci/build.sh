#!/bin/bash -e
PROJ_ROOT=$(realpath "$(dirname $0)""/..")
SRC_ROOT=${PROJ_ROOT}/src
WORK_DIR=${PROJ_ROOT}/temp/build
rm -rf ${WORK_DIR}
mkdir -p ${WORK_DIR}
cd ${WORK_DIR}
cmake ${SRC_ROOT}
make -j $(nproc) 
