#include <stdio.h>
#include "sample.h"
using namespace std;

#define VAL_ZERO 0

int main(){
  int v = 18;
  printf("Invalid division %d\n", v / VAL_ZERO);
  printf("Invalid modulo %d\n", v % VAL_ZERO);
  
  int uninitialized;
  printf("Uninitialized %d\n", uninitialized);

  int *pint = NULL;
  printf("NULL pointer %d\n", *pint);

  short s[12];
  for(int i=0; i<=12; ++i){
    s[i] = 0;
  }
  
  int overflow = 2147483647;
  printf("Integer overflow %d\n", overflow+1);

  int underflow = -2147483648;
  printf("Integer overflow %d\n", underflow-1);

  return 0;
}
